import XCTest
@testable import SwiftFormatterPlugin

final class SwiftFormatterPluginTests: XCTestCase {
    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct
        // results.
        XCTAssertEqual(SwiftFormatterPlugin().text, "Hello, World!")
    }
}
