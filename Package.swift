// swift-tools-version: 5.6
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "SwiftFormatterPlugin",
    products: [
        // Products define the executables and libraries a package produces, and make them visible to other packages.
        .library(name: "PluginLibrary", targets: ["PluginLibrary"]),
        .plugin(name: "FormatLintSource", targets: ["FormatLintSource"])
    ],
    dependencies: [
        // Dependencies declare other packages that this package depends on.
        // .package(url: /* package url */, from: "1.0.0"),
        .package(url: "https://github.com/apple/swift-format.git", branch: "release/5.6"),
        .package(url: "https://github.com/apple/swift-argument-parser.git", .upToNextMinor(from: "1.1.1")),
    ],
    targets: [
        // Targets are the basic building blocks of a package. A target can define a module or a test suite.
        // Targets can depend on other targets in this package, and on products in packages this package depends on.
        .target(
            name: "PluginLibrary",
            dependencies: [
                .product(name: "SwiftFormat", package: "swift-format"),
                .product(name: "SwiftFormatConfiguration", package: "swift-format"),
                .product(name: "ArgumentParser", package: "swift-argument-parser")

            ]),
        .plugin(
            name: "FormatLintSource", 
            capability: .command(
                intent: .custom(verb: "formatter", description: "Formats Swift code using Apple's swift-format"), 
                permissions: [
                    .writeToPackageDirectory(reason: "This command reformats source files")
                ]
            ),
            dependencies: [
                .product(name: "PluginLibrary", package: "SwiftFormatterPlugin"),
            ]
        ),
        .testTarget(
            name: "SwiftFormatterPluginTests",
            dependencies: ["PluginLibrary"]),
    ]
)
